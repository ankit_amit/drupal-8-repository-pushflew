<?php

/**
 * @file
 * Contains \Drupal\mymodule\EventSubscriber\MyModuleSubscriber.
 */

// Declare the namespace for our own event subscriber.

namespace Drupal\pushflew_commerce\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\commerce_cart\Event\CartEntityAddEvent;
use Drupal\commerce_cart\Event\CartOrderItemUpdateEvent;
use Drupal\commerce_cart\Event\CartOrderItemRemoveEvent;
use Drupal\commerce_cart\Event\CartEvents;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\Core\Url;

class pushflewCommerceSubscriber implements EventSubscriberInterface
{


    protected $account;

    protected $pushFlowApiUrl;

    protected $websiteId;

    protected $authorization = "Bearer 5abcea9f5caf316920643e079056bae905ca7443a703ffc21cdd706c5fcfde3378b3a1c75de1436863e26e0c";

    private $orderCompleted = 0;

    private $productImageSend = 'dot.png';


    public function __construct(AccountInterface $account)
    {
        $this->account = $account;
    }


    public static function getSubscribedEvents()
    {
        $events = [
            CartEvents::CART_ENTITY_ADD => 'sendPushFlewAddResponse', CartEvents::CART_ORDER_ITEM_UPDATE => 'sendPushFlewUpdateResponse', CartEvents::CART_ORDER_ITEM_REMOVE => 'sendPushFlewRemoveResponse', 'commerce_order.place.post_transition' => 'sendPushFlewOrderResponse',
        ];
        return $events;
    }


    public function sendPushFlewAddResponse(CartEntityAddEvent $event, $eventName)
    {
        $dataCart = $this->getTotalQuantityPrice($event);
        $this->sendApiDataPushFlow($eventName, $dataCart);
    }

    public function sendPushFlewUpdateResponse(CartOrderItemUpdateEvent $event, $eventName)
    {
        $dataCart = $this->getTotalQuantityPrice($event);
        $this->sendApiDataPushFlow($eventName, $dataCart);
        $dataEmailNotification = $this->getDataEachProduct($event);
        $this->sendEmailNotification($eventName, $dataEmailNotification);
    }

    public function sendPushFlewRemoveResponse(CartOrderItemRemoveEvent $event, $eventName)
    {
        $dataCart = $this->getTotalQuantityPrice($event);
        $this->sendApiDataPushFlow($eventName, $dataCart);
        $dataEmailNotification = $this->getDataEachProduct($event);
        $this->sendEmailNotification($eventName, $dataEmailNotification);

    }

    public function sendPushFlewOrderResponse(WorkflowTransitionEvent $event, $eventName)
    {
        $hostUrl = \Drupal::request()->getHost();
        $dataCart = array('cart_quantity' => '0', 'cart_total' => '0', 'cart_days' => '0');
        $this->orderCompleted = 1;
        $this->sendApiDataPushFlow($eventName, $dataCart);
        $dataEmailNotification = array('cart_quantity' => '0', 'cart_total' => '0', 'cart_days' => '0', 'cart_link' => $hostUrl.Url::fromRoute('commerce_cart.page')->toString(), 'cart_items' => array());
        $this->sendEmailNotification($eventName, $dataEmailNotification);

    }

    private function getTotalQuantityPrice($event)
    {

        $totalPrice = 0;
        $totalQuantity = 0;
        $getItems = $event->getCart()->getItems();

        foreach ($event->getCart()->getItems() as $order_item) {
            $totalQuantity += (int)$order_item->getQuantity();
            $totalPrice = $totalPrice + $order_item->getTotalPrice()->getNumber();
        }
        return array('cart_quantity' => $totalQuantity, 'cart_total' => $totalPrice, 'cart_days' => '0');
    }

    public function getDataEachProduct($event)
    {

        $getItems = $event->getCart()->getItems();

        $cartItems = array();

        $hostUrl = \Drupal::request()->getHost();

        $currency_importer = \Drupal::service('commerce_price.currency_importer');
        $totalPrice = 0;
        $totalQuantity = 0;
        foreach ($event->getCart()->getItems() as $order_item) {

            $quantityProduct = (int)$order_item->getQuantity();
            $totalQuantity += $quantityProduct;


            $priceProduct = number_format($order_item->getTotalPrice()->getNumber(),2);
            $newPriceProduct = str_replace(",","",$priceProduct);
            $totalPrice = $totalPrice + $newPriceProduct;

            $unitPriceProduct = number_format($order_item->getUnitPrice()->getNumber(),2);
            $priceCurrencyCode = $order_item->getUnitPrice()->getCurrencyCode();

            $priceCurrency =  $currency_importer->import($priceCurrencyCode)->getSymbol();
            

            $imageUrl = $hostUrl.base_path().drupal_get_path('module', 'pushflew_commerce') ."/images/".$this->productImageSend;

            $cartItems[] = array('name' => strval($order_item->getTitle()), 'imageURL' => $imageUrl, 'quantity' => $quantityProduct, 'price' => $priceCurrency ." ".$unitPriceProduct, 'subTotal' => $priceCurrency ." ". $priceProduct);

        }


        $cartItems = array('cart_quantity' => $totalQuantity, 'cart_total' => $priceCurrency . $totalPrice, 'cart_days' => '0', 'cart_link' => $hostUrl . Url::fromRoute('commerce_cart.page')->toString(), 'cart_items' => $cartItems);


        return $cartItems;
    }

    private function sendApiDataPushFlow($eventName, $dataSent)
    {

        $configSettings = $this->getConfigSettings();
        $this->websiteId = $configSettings['websiteId'];
        $this->pushFlowApiUrl = $configSettings['pushFlowApiUrl'];


        $email = $this->getEmailCurrentUser();

        $subscriberId = $this->getSubscriberId();

        if( $email != '' || $subscriberId != '' ) {

            $subscriber = array('subscriberId' => $subscriberId, 'email' => $email, 'phone' => '');
            $eventPushFlow = $this->getEventNamePushFlow($eventName, $dataSent);
            $data = array('websiteId' => $this->websiteId, 'subscriber' => $subscriber, 'eventName' => $eventPushFlow, 'data' => $dataSent);
            $this->sendApiRequest($this->pushFlowApiUrl, $data);
        }

    }

    private function getConfigSettings()
    {
        $configSettings = \Drupal::config('pushflew_commerce.settings');
        $this->websiteId = $configSettings->get('pushFlewEventSubscriber.websiteId');
        $this->pushFlowApiUrl = $configSettings->get('pushFlewEventSubscriber.pushFlowApiUrl');
        $this->emailNotificationUrl = $configSettings->get('pushFlewEventSubscriber.emailApiUrl');
        return array('websiteId' => $this->websiteId, 'pushFlowApiUrl' => $this->pushFlowApiUrl, 'emailNotificationUrl' => $this->emailNotificationUrl);


    }

    private function getEmailCurrentUser()
    {

        $idCurrentUser = $this->account->id();

        if( $idCurrentUser )
            $email = $this->account->getEmail();

        return $email;

    }

    private function getSubscriberId()
    {

        global $_COOKIE;

        $subscriberKey = "pushflew-subscriber-id-" . $this->websiteId;
        $subscriberId = $_COOKIE[$subscriberKey];
        return $subscriberId;

    }

    private function getEventNamePushFlow($eventName, $dataSent)
    {

        if( $this->orderCompleted )
            return 'order_placed';
        else if( empty($dataSent['cart_quantity']) && empty($dataSent['cart_total']) )
            return 'cart_cleared';
        else if( $eventName == 'commerce_cart.entity.add' || $eventName == 'commerce_cart.order_item.update' || $eventName == 'commerce_cart.order_item.remove' )
            return 'cart_abandonment';

    }

    protected function sendApiRequest($url, $data)
    {

  
        $response = \Drupal::httpClient()->post($url, [
            'json' => $data,
            'headers' => [
                'Content-type' => 'application/json', 'Authorization' => $this->authorization,
            ],
        ]);

    }

    public function sendEmailNotification($eventName, $dataEmailNotification)
    {


        $configSettings = $this->getConfigSettings();

        $this->websiteId = $configSettings['websiteId'];
        $this->emailApiUrl = $configSettings['emailNotificationUrl'];

        $subscriberId = $this->getEmailSubscriberId();
        $email = $this->getEmailCurrentUser();

        $eventPushFlow = $this->getEventNamePushFlow($eventName, $dataEmailNotification);

        if( $email != '' || $subscriberId != '' ) {

            $dataToSend = array('websiteId' => $this->websiteId, 'eventName' => $eventPushFlow, 'subscriber' => array('subscriberId' => $subscriberId, 'email' => $email, 'phone' => ''), 'data' => $dataEmailNotification);

            $jsonData = json_encode($dataToSend);
            $this->sendApiRequest($this->emailApiUrl, $dataToSend);
        }

    }

    private function getEmailSubscriberId()
    {

        global $_COOKIE;

        $subscriberKey = "pushflew-email-subscriber-id-" . $this->websiteId;
        $subscriberId = $_COOKIE[$subscriberKey];
        return $subscriberId;
    }
}





