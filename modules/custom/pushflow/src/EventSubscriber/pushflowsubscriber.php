<?php

/**
 * @file
 * Contains \Drupal\mymodule\EventSubscriber\MyModuleSubscriber.
 */

// Declare the namespace for our own event subscriber.

namespace Drupal\pushflow\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\commerce_cart\Event\CartEntityAddEvent;
use Drupal\commerce_cart\Event\CartOrderItemUpdateEvent;
use Drupal\commerce_cart\Event\CartOrderItemRemoveEvent;
use Drupal\commerce_cart\Event\CartEvents;
use Drupal\state_machine\Event\WorkflowTransitionEvent;


class pushflowsubscriber implements EventSubscriberInterface
{


    protected $account;

    protected $pushFlowApiUrl;

    protected $websiteId;

    protected $authorization = "Bearer 5b07f10cbeda463bed4f589aad4d05a339993305a10e1c48fcfcbc58cf17102dfcda7d5fa5b4c112b5c2a285";

    private $orderCompleted = 0;

    public function __construct(AccountInterface $account)
    {
        $this->account = $account;
    }


    public static function getSubscribedEvents()
    {
        $events = [
            CartEvents::CART_ENTITY_ADD => 'sendPushFlowAddResponse', CartEvents::CART_ORDER_ITEM_UPDATE => 'sendPushFlowUpdateResponse', CartEvents::CART_ORDER_ITEM_REMOVE => 'sendPushFlowRemoveResponse', 'commerce_order.place.post_transition' => 'sendPushFlowOrderResponse',
        ];
        return $events;
    }


    public function sendPushFlowAddResponse(CartEntityAddEvent $event, $eventName)
    {
        $dataCart = $this->getTotalQuantityPrice($event);
        $this->sendApiDataPushFlow($eventName, $dataCart);
    }

    private function getTotalQuantityPrice($event)
    {

        $getItems = $event->getCart()->getItems();

        foreach ($event->getCart()->getItems() as $order_item) {
            $totalQuantity += (int)$order_item->getQuantity();
            $totalPrice = $totalPrice + $order_item->getTotalPrice()->getNumber();
        }
        return array('cart_quantity' => $totalQuantity, 'cart_total' => $totalPrice, 'cart_days' => '0');
    }

    private function sendApiDataPushFlow($eventName, $dataSent)
    {

        $configSettings = \Drupal::config('pushflow.settings');
        $this->websiteId = $configSettings->get('pushFlowEventSubscriber.websiteId');
        $this->pushFlowApiUrl = $configSettings->get('pushFlowEventSubscriber.pushFlowApiUrl');


        $email = $this->getEmailCurrentUser();

        $subscriberId = $this->getSubscriberId();

        if( $email != '' || $subscriberId != '' ) {

            $subscriber = array('subscriberId' => $subscriberId, 'email' => $email, 'phone' => '');

            $eventPushFlow = $this->getEventNamePushFlow($eventName, $dataSent);


            $data = array('websiteId' => $this->websiteId, 'subscriber' => $subscriber, 'eventName' => $eventPushFlow, 'data' => $dataSent);


            $options = array(
                'method' => 'POST',
                'data' => json_encode($data),
                'headers' => array('Content-Type' => 'application/json', 'Authorization' => 'Bearer 5abcea9f5caf316920643e079056bae905ca7443a703ffc21cdd706c5fcfde3378b3a1c75de1436863e26e0c')
            );


            $response = \Drupal::httpClient()->post($this->pushFlowApiUrl, [
                'json' => $data,
                'headers' => [
                    'Content-type' => 'application/json', 'Authorization' => $this->authorization,
                ],
            ]);


        }

    }

    private function getEmailCurrentUser()
    {

        $idCurrentUser = $this->account->id();

        if( $idCurrentUser )
            $email = $this->account->getEmail();

        return $email;

    }

    private function getSubscriberId()
    {

        global $_COOKIE;

        $subscriberKey = "pushflew-subscriber-id-" . $this->websiteId;
        $subscriberId = $_COOKIE[$subscriberKey];
        return $subscriberId;

    }


    private function getEventNamePushFlow($eventName, $dataSent)
    {

        if( $this->orderCompleted )
            return 'order_placed';
        else if( empty($dataSent['cart_quantity']) && empty($dataSent['cart_total']) )
            return 'cart_cleared';
        else if( $eventName == 'commerce_cart.entity.add' || $eventName == 'commerce_cart.order_item.update' || $eventName == 'commerce_cart.order_item.remove' )
            return 'cart_abandonment';

    }

    public function sendPushFlowUpdateResponse(CartOrderItemUpdateEvent $event, $eventName)
    {
        $dataCart = $this->getTotalQuantityPrice($event);
        $this->sendApiDataPushFlow($eventName, $dataCart);
    }

    public function sendPushFlowRemoveResponse(CartOrderItemRemoveEvent $event, $eventName)
    {
        $dataCart = $this->getTotalQuantityPrice($event);
        $this->sendApiDataPushFlow($eventName, $dataCart);
    }

    public function sendPushFlowOrderResponse(WorkflowTransitionEvent $event, $eventName)
    {
        $dataCart = array('cart_quantity' => '0', 'cart_total' => '0', 'cart_days' => '0');
        $this->orderCompleted = 1;
        $this->sendApiDataPushFlow($eventName, $dataCart);
    }


}





